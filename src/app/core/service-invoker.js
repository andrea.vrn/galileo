
/*

Usage in service:

function loadContacts() {
  return serviceInvoker.callRegistered('user-contacts');
}

function createNewContact(contact) {
  return serviceInvoker.callRegistered('user-contacts-create', {payload: contact});
}

Usage in controller:

contactsService.loadContacts()
// o direttamente: serviceInvoker.callRegistered('user-contacts')
.then(
  function(response) {
    console.log('user-contacts success ', response);
    $scope.contacts = response.data;
  },
  function(response) {
    console.log('user-contacts error ', response);
    errorHandler.notify(response);
  }
);

*/

(function(window) {
  'use strict';
  var angular = window.angular;
  angular.module('galileo.core').factory('serviceInvoker', serviceInvoker);

  serviceInvoker.$inject = ['$http', '$q', 'serviceCatalogue', 'langUtils', 'errorHandler'];

  function serviceInvoker($http, $q, serviceCatalogue, utils, errorHandler) {
    var baseEnvelop = {
      headers:{
        timestamp: utils.timestamp()
      }
    };

    var service = {
      callRegistered: callRegistered,
      callUrl: callUrl
    };

    return service;

    function callRegistered(serviceId, config) {
      // if (utils.isEmptyObject(config)) {
      //   errorHandler.swallow('empty message');
      // }
      var message = config || {};
      var service = serviceCatalogue.getServiceCoords(serviceId);
      if (!service) {
        errorHandler.swallow('no service registered for key ' + serviceId);
        return  rejectedPromise('error service-id');
      }
      var data = buildMessage(message);
      var path = message.path || '';
      return callHttp({
        method: service.httpMethod,
        url: service.endpoint + path,
        headers: {
          'Content-Type': 'application/json'
        },
        data: data.payload
      });
    }

    function callUrl(serviceUrl, httpConfig) {
      var config = httpConfig || {};
      config.url = serviceUrl;
      config.method = config.method || 'GET';
      return callHttp(config);
    }

    function callHttp(config) {
      var deferred = $q.defer();
      deferred.notify('Call HTTP:', config);
      var headers = config.headers;
      var url = config.url;
      $http({
        method: config.method,
        url: url,
        data: config.data,
        headers: headers
      }).
      then(function(response, status, headers, config) {
        var data = response.data;
        deferred.notify('Received response from url: ' + url + '; data: ' + data);
        var accepted = validateHttpResponse(data, status, headers, config);
        if ( accepted ) {
          deferred.notify('Accepted response from url: ' + url);
          deferred.resolve({data: data, status: status});
        } else {
          deferred.notify('Rejected response from url: ' + url);
          deferred.reject( status + ' - Unexpected status code.');
        }
      }, function(data, status, headers, config) {
        /*jshint unused:false */
        deferred.reject( { data:data, status:status } );
      });
      return ( deferred.promise );
    }

    function validateHttpResponse(data, status, headers, config) {
      /*jshint unused:false */
      return true;
    }

    function rejectedPromise(data) {
      var deferred = $q.defer();
      deferred.reject( { data:data } );
      return ( deferred.promise );
    }

    function buildMessage(message) {
      var payload = message.payload || {};
      var extraheaders = message.headers || {};
      var m = baseEnvelop;
      m.payload = payload;
      var h = angular.merge(m.headers, extraheaders);
      m.headers = h;
      return m;
    }
  }
})(window);
