(function() {
  'use strict';

  angular
  .module('galileo.core')
  .factory('appProperties', appProperties);

  /**
  * @ngdoc factory
  * @name it.ifis.arch.fe.app:appProperties
  *
  * @description
  * A service to manage the application properties (both architectural and
  * application one) and to load them before bootstrap.
  *
  * @ngInject
  */
  function appProperties($http, APP_SETUP) {

    var properties = null;

    return {
      loadProperties: loadProperties,
      getProperty: getProperty
    };

    /**
    * @ngdoc method
    * @name loadProperties
    * @methodOf it.ifis.arch.fe.app:appProperties
    *
    * @description
    * Load the application properties and store them in a local variable.
    */
    function loadProperties() {
      return $http.get(APP_SETUP.propertiesEndpoint)
      .success(function(response) {
        console.log("properties loaded =", response);
        properties = response;
        return;
      })
      .error(function(error) {
        console.error('error loading properties =', error);
        return;
      });
    }

    /**
    * @ngdoc method
    * @name getProperty
    * @methodOf it.ifis.arch.fe.app:appProperties
    *
    * @description
    * Get a property by key from the local variable.
    *
    * @param {string} key
    *
    * @returns the property value associated to the key
    */
    function getProperty(key) {
      var result = properties;

      var keyTokens = key.split('.');
      for (var i = 0; i < keyTokens.length; i++) {
        result = result[keyTokens[i]];
      }

      return result;
    }
  }

})();
