(function(window) {
    'use strict';
    var angular = window.angular;
    var console = window.console;

    angular
        .module('galileo.core')
        .factory('serviceCatalogue', serviceCatalogue);

    serviceCatalogue.$inject = ['configuration'];

    function serviceCatalogue(configuration) {

        var config = configuration.get('internalServices');
        var internalServicesBaseUrl = config.baseUrl;
        var contactsEndpoint = internalServicesBaseUrl + 'contacts';
        var catalogue = {
            'user-agent': {
                endpoint: 'https://httpbin.org/user-agent',
                httpMethod: 'GET'
            },
            /*
            'user-account': {
                endpoint: internalServicesBaseUrl + 'data/account.json',
                httpMethod: 'GET'
            },
            'user-operations': {
                endpoint: internalServicesBaseUrl + 'data/operations.json',
                httpMethod: 'GET'
            },
            */
            'user-contacts': {
                endpoint: contactsEndpoint,
                httpMethod: 'GET'
            },
            'user-contacts-details': {
                endpoint: contactsEndpoint,
                httpMethod: 'GET'
            },
            'user-contacts-create': {
                endpoint: contactsEndpoint,
                httpMethod: 'POST'
            },
            'user-contacts-edit': {
                endpoint: contactsEndpoint,
                httpMethod: 'PUT'
            },
            'user-contacts-delete': {
                endpoint: contactsEndpoint,
                httpMethod: 'DELETE'
            }
        };

        var service = {
            getServiceCoords: getServiceCoords
        };

        return service;

        function getServiceCoords(serviceId) {
            var s = catalogue[serviceId];
            if (!s) {
                console.error('Service NOT found: ', serviceId);
            }
            return s;
        }
    }

})(window);
