(function(window) {
  'use strict';
  var angular = window.angular;
  var console = window.console;

  angular
  .module('galileo.core')
  .factory('errorHandler', errorHandler);

  errorHandler.$inject = ['$http', '$state'];

  function errorHandler($http, $state) {
    var ERROR = 'ERROR';
    var service = {
      goError: goError,
      swallow: swallow,
      notify: notify
    };

    return service;

    function swallow(error) {
      console.warn('swallow error ', error);
    }

    function goError(state, message) {
      setSessionProperty(ERROR, message);
      $state.go(state);
    }

    function notify(error) {
      var payload = buildErrorPayload(error);
      setSessionProperty(ERROR, payload);
      $state.go('error', {error:payload});
    }

    function setSessionProperty(key, value) {
      // var data = value || {};
      // data.timestamp = timestamp();
      return sessionStorage.setItem(key, JSON.stringify(value));
    }

    function timestamp() {
      return new Date().getTime();
    }

    function buildErrorPayload(error) {
      var e = {};
      if (typeof error === 'string') {
        e.message = error;
      } else {
        e.details = error;
      }
      e.timestamp = timestamp();
      return e;
    }
  }

})(window);
