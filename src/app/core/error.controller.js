(function(window) {
  'use strict';
  var angular = window.angular;
  angular.module('galileo.core')
  .controller('ErrorCtrl', ErrorCtrl);

  ErrorCtrl.$inject = ['$scope', '$stateParams'];
  function ErrorCtrl($scope, $stateParams) {
    console.log('got error payload: ', $stateParams.error);
    $scope.error = $stateParams.error || 'Ooops! Something gone wrong!';
  }

})(window);
