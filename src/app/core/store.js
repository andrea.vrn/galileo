(function(window) {
  'use strict';
  var angular = window.angular;

  angular
  .module('galileo.core')
  .factory('store', store);

  store.$inject = ['errorHandler'];

  function store(errorHandler) {

    var PROCESS_LIST           = 'PROCESS_LIST';
    var LANDING_CONFIGURATION  = 'LANDING_CONFIGURATION';

    var service = {
      getProcessConfiguration: getProcessConfiguration,
      getLandingConfiguration: getLandingConfiguration,
      getSessionProperty: getSessionProperty,
      setSessionProperty: setSessionProperty
    };

    return service;

    function getProcessConfiguration() {
      return JSON.parse(sessionStorage.getItem(PROCESS_LIST));
    }

    function getLandingConfiguration() {
      return JSON.parse(sessionStorage.getItem(LANDING_CONFIGURATION));
    }

    function getSessionProperty(key) {
      var value = sessionStorage.getItem(key);
      if (!value) {
        errorHandler.swallow('store value not found for key ' + key);
        return null;
      }
      return JSON.parse(value);
    }

    function setSessionProperty(key, data) {
      return sessionStorage.setItem(key, JSON.stringify(data));
    }


  }

})(window);
