(function(window) {
    'use strict';
    var angular = window.angular;
    var console = window.console;

    angular
        .module('galileo.core')
        .factory('configuration', configuration);

    configuration.$inject = ['APP_SETUP', 'store'];

    function configuration(APP_SETUP, store) {

        var appConfiguration;

        var service = {
            get: get
        };

        return service;

        // FIXME: now it returns only first level properties
        function get(key) {
            if (!appConfiguration) {
                appConfiguration = loadConfiguration();
            }
            console.warn('configuration looking for ', key, ' in ', appConfiguration);
            return appConfiguration[key];
        }

        function loadConfiguration() {
            return store.getSessionProperty(APP_SETUP.configurationStorageKey);
        }
    }

})(window);
