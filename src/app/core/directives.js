
(function(window) {
  'use strict';
  var angular = window.angular;

  angular.module('galileo.core')
  .directive('appNotifications', appNotifications);

  function appNotifications($sce) {

    return {
        restrict : "E",
        template : '<div class="{{ cssClass }}">Notification: <pre>{{ notification }}</pre><a ng-click="close()">Close</a></div>',
        link : function (scope) {
            scope.cssClass = 'ng-hide';
            scope.close  = function () {
              console.log("close  clicked");
              scope.cssClass = 'ng-hide';
            };
            scope.$on("app.notification", function (event, args) {
                scope.notification = args;
                scope.cssClass = 'error';
              //  scope.html = $sce.trustAsHtml('<div>Notification: <pre>'+args+'</pre></div>');
            });
        }
    };

    function close() {
      alert('ciao');
    }
  }

})(window);
