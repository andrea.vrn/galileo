'use strict';

/**
 * @ngdoc function
 * @name iteconsappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the iteconsappApp
 */
angular.module('galileo.core')
  .controller('HomeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
