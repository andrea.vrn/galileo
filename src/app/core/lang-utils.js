(function(window) {
  'use strict';
  var angular = window.angular;

  angular
  .module('galileo.core')
  .factory('langUtils', langUtils);

  function langUtils() {

    var service = {
      isEmptyObject: isEmptyObject,
      isNumeric: isNumeric,
      timestamp: timestamp
    };

    return service;

    function isEmptyObject(obj) {
      if (!obj) {
        return true;
      }
      for(var prop in obj) {
        if(obj.hasOwnProperty(prop)) {
          return false;
        }
      }
      return true && JSON.stringify(obj) === JSON.stringify({});
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function timestamp() {
      return new Date().getTime();
    }
  }
})(window);
