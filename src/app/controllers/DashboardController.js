(function () {
    angular
        .module('app')
        .controller('DashboardController', [
          'dashboardService',
          '$scope',
          '$interval',
          DashboardController
        ])
        .filter('cloneAttivoFilter', function() {
          return function(items, statoClone) {
            var filtered = [];
            angular.forEach(items, function(clone) {
              if(clone.active == statoClone) {
                filtered.push(clone);
              }
            });
            return filtered;
          };
        })
        .filter('capitalize', function() {
            return function(input) {
              return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
            }
        })
        .filter('stageFilter', function() {
          return function(items, stageLabel) {
            var filtered = [];
            if (stageLabel == "")
              return items;
            angular.forEach(items, function(clone) {
              if(clone.stage == stageLabel) {
                filtered.push(clone);
              }
            });
            return filtered;
          };
        })
        .filter('cloneSelectedFilter', function() {
          return function(items, cloneSlctd) {
            var filtered = [];
            if (cloneSlctd.length == 0)
              return items;
            angular.forEach(items, function(clone) {
              if(clone.stage == cloneSlctd.stage && clone.codice_clone == cloneSlctd.codice_clone) {
                filtered.push(clone);
              }
            });
            return filtered;
          };
        })
        .filter('spAperteFilter', function() {
          return function(items, vediChiusePar) {
            var filtered = [];
            angular.forEach(items, function(richiesta) {
              if(vediChiusePar){
                  filtered.push(richiesta);
              }
              else if(richiesta.stato_richiesta == 1){
                filtered.push(richiesta);
              }
            });
            return filtered;
          };
        });

    function DashboardController(dashboardService, $scope, $interval) {

      var REFRESH_INTERVAL = 100000; //milliseconds

      $scope.resizeGridValuesActive = {
        "mdcolssm": 2,
        "mdcolsmd": 8,
        "mdcolslg": 12,
        "mdcolsgtlg": 24,
      };

      $scope.cloneSelected = [];
      $scope.cloni = [];

      $scope.stageList = [];
      $scope.stageSelected = "";

      $scope.idSelectedSP = null;
      $scope.idSelectedDBR = null;
      $scope.SPlistByClone = [];
      $scope.SPlistByCloneFiltered = [];
      $scope.viewInfo = false;
      $scope.viewInfoIcon = "info_outline";
      $scope.vediChiuse = false;
      $scope.querySPlistByClone = {
        order: 'richiesta_sp',
        limit: 10,
        page: 1,
        total: 0
      };
      $scope.options = {
        rowSelection: true,
        multiSelect: false,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: true,
        limitSelect: true,
        pageSelect: true
      };

      $scope.spSelected = [];
      $scope.queryGetDBdataBySp = {
        order: 'oggetto',
        limit: 10,
        page: 1,
        total: 0
      };
      $scope.oggettiBySP = {
        "db2": [],
        "vsm": [],
        "seq": [],
        "gdg": []
      };

      $scope.dbrSelected = [];
      $scope.TSlistByDBR = [];
      $scope.queryGetTSbyDBR = {
        order: 'tablespace',
        limit: 10,
        page: 1,
        total: 0
      };

      // --- initialization logic
      init();

      var stop;
      function init() {
        // Don't start a new fight if we are already fighting
        if ( angular.isDefined(stop) ) return;
        appExecutionFlow();
        stop = $interval(appExecutionFlow, REFRESH_INTERVAL);
      };

      function stopInterval() {
        if (angular.isDefined(stop)) {
          $interval.cancel(stop);
          stop = undefined;
        }
      };

      function appExecutionFlow(){
        getStageList();
        getCloni();
      }

      function getCloni(){
        var cloniTmp = [];
        dashboardService.getData("GetCloni").then(function(response){
          cloniTmp = JSON.parse(response.data);
          //cloniTmp = response.data;

          $scope.cloni = [];

          for (var i = 0; i < cloniTmp.length; i++) {
            $scope.cloni.push({
              id: cloniTmp[i].$id,
              codice_clone: cloniTmp[i].CODICE_CLONE,
              desc_clone: cloniTmp[i].DESC_CLONE,
              stage: cloniTmp[i].STAGE,
              db2: cloniTmp[i].DB2,
              stato_clone: getStatoEsteso(cloniTmp[i].STATOCLONE_CALCOLATO, cloniTmp[i].STATO_ONDEMAND),
              stato_ondemand: getStatoOnDemand(cloniTmp[i].STATO_ONDEMAND),
              richieste_attive: cloniTmp[i].RICHIESTE_ATTIVE,
              richieste_chiuse: cloniTmp[i].RICHIESTE_CHIUSE,
              color: getCloneColor(cloniTmp[i].STATOCLONE_CALCOLATO),
              colspan: cloniTmp[i].RICHIESTE_ATTIVE > 0 ? 2 : 1,
              rowspan: cloniTmp[i].RICHIESTE_ATTIVE > 0 ? 2 : 1,
              active: cloniTmp[i].RICHIESTE_ATTIVE > 0 ? true : false,
              data_zoccolo_duro: cloniTmp[i].DATA_ZOCCOLO_DURO,
              data_foto: cloniTmp[i].DATA_FOTO,
              data_allestimento: cloniTmp[i].DATA_ALLESTIMENTO,
              data_dismissione: cloniTmp[i].DATA_DISMISSIONE,
              data_popolamento: cloniTmp[i].DATA_POPOLAMENTO
            });
          }
        });
      }

      function getStageList(){
        var cloniTmp = [];
        dashboardService.getData("GetStageList").then(function(response){
          $scope.stageList = JSON.parse(response.data);
          //$scope.stageList = response.data;
          console.log('stagelist', $scope.stageList)
        });
      }

      function getSPbyClone(cloneIN, stageIN){
        var SPlistByCloneTmp = [];
        dashboardService.getData("GetSPbyClone/" + cloneIN + "/" + stageIN).then(function(response){

          SPlistByCloneTmp = JSON.parse(response.data);
          //cloniTmp = response.data;

          $scope.SPlistByClone = [];

          for (var i = 0; i < SPlistByCloneTmp.length; i++) {
            $scope.SPlistByClone.push({
              id: SPlistByCloneTmp[i].ID,
              richiesta_sp: SPlistByCloneTmp[i].RICHIESTA_SP,
              stage: SPlistByCloneTmp[i].STAGE_ARRIVO,
              clone: SPlistByCloneTmp[i].CLONE_ARRIVO,
              attuatore: SPlistByCloneTmp[i].USERID_ATTUATORE,
              stato_richiesta: SPlistByCloneTmp[i].STATO_RICHIESTA,
              inizio_attivita: SPlistByCloneTmp[i].INIZIO_ATTIVITA,
              fine_attivita: SPlistByCloneTmp[i].FINE_ATTIVITA,
              tipo_attivita: SPlistByCloneTmp[i].TIPO_ATTIVITA,
              tracciatore: SPlistByCloneTmp[i].TRACCIATORE,
              sum_stato_c: SPlistByCloneTmp[i].SUM_STATO_C,
              sum_stato_tot: SPlistByCloneTmp[i].SUM_STATO_C + SPlistByCloneTmp[i].SUM_STATO_NON_C,
              perc_avanvamento: getPercAvanzamento(
                SPlistByCloneTmp[i].SUM_STATO_C,
                SPlistByCloneTmp[i].SUM_STATO_NON_C,
                SPlistByCloneTmp[i].COUNT_STATO_C,
                SPlistByCloneTmp[i].COUNT_STATO_NON_C
              )/*
              perc_avanvamento: getPercAvanzamento(
                150,
                30,
                80,
                5
              )*/
            });
          }
        });
      }

      function getDBdataBySp(idSP){
        var oggettiBySPTmp = [];
        dashboardService.getData("GetDBdataBySp/" + idSP).then(function(response){

          oggettiBySPTmp = JSON.parse(response.data);
          //cloniTmp = response.data;

          $scope.oggettiBySP = {
            "db2": [],
            "vsm": [],
            "seq": [],
            "gdg": []
          };

          for (var i = 0; i < oggettiBySPTmp.length; i++) {
            var oggettoTmp = {
              id_richiesta: oggettiBySPTmp[i].ID_RICHIESTA,
              oggetto: oggettiBySPTmp[i].OGGETTO,
              ssa: oggettiBySPTmp[i].CODICE_SSA,
              ssa_desc: oggettiBySPTmp[i].DESCRIZIONE_SSA,
              acronimo: oggettiBySPTmp[i].CODICE_ACRONIMO
            };

            switch(oggettiBySPTmp[i].TIPO_OGGETTO){
              case 'DB2':
                $scope.oggettiBySP.db2.push(oggettoTmp);
                break;
              case 'VSM':
                $scope.oggettiBySP.vsm.push(oggettoTmp);
                break;
              case 'GDG':
                $scope.oggettiBySP.gdg.push(oggettoTmp);
                break;
              case 'SEQ':
                $scope.oggettiBySP.seq.push(oggettoTmp);
                break;
              default:
                $scope.oggettiBySP.db2.push(oggettoTmp);
                break;
            }


          }
        });
      }

      function getTSbyDBR(oggettoDBR){
        var TSbyDBRTmp = [];
        dashboardService.getData("GetTSByDBR/" + oggettoDBR).then(function(response){

          TSbyDBRTmp = JSON.parse(response.data);
          //cloniTmp = response.data;

          $scope.TSlistByDBR = [];

          for (var i = 0; i < TSbyDBRTmp.length; i++) {
            $scope.TSlistByDBR.push({
              id: TSbyDBRTmp[i].$id,
              tablespace: TSbyDBRTmp[i].TABLESPACE,
              stato: TSbyDBRTmp[i].STATO,
              spazio: TSbyDBRTmp[i].SPAZIO
            });
          }

          //console.log('TSlistByDBR', $scope.TSlistByDBR);
        });
      }

      //var COLORS = ['#d11141','#00b159','#00aedb','#f37735','#ffc425'];
      //            red       green     blue        orange       yellow

      var COLORS = ['#005FDB', '#2FD8E0', '#C4C4C4', '#FFC425', '#00B159'];
      //            BLU       AZZURRO     GRIGIO        GIALLO       VERDE

      function getCloneColor(stato) {
        var indexColor = 0;
        switch(stato){
          case "IC_M":
            indexColor = 0;
            break;
          case "IC_G":
            indexColor = 1;
            break;
          case "ID":
            indexColor = 2;
            break;
          case "IA":
            indexColor = 3;
            break;
          case "AT":
            indexColor = 4;
            break;
          default:
            break;
        }
        return COLORS[indexColor];
      }

      function getStatoEsteso(stato, ondemand) {
        var statoEsteso = "";
        switch(stato){
          case "IC_M":
            statoEsteso = "Attività massiva in corso";
            break;
          case "IC_G":
            statoEsteso = "Attività generica in corso";
            break;
          case "ID":
            statoEsteso = "Clone in dismissione";
            break;
          case "IA":
            statoEsteso = "Clone in allestimento";
            break;
          case "AT":
            statoEsteso = "Nessuna attività su questo clone";
            break;
          default:
            break;
        }

        switch(ondemand){
          case "OD_AC":
            statoEsteso += " (clone on-demand acceso)";
            break;
          case "OD_SP":
            statoEsteso += " (clone on-demand spento)";
            break;
          default:
            break;
        }

        return statoEsteso;
      }

      function getStatoOnDemand(stato) {
        var statoOnDemand = "";
        switch(stato){
          case "OD_AC":
            statoOnDemand = "ondemand_attivo";
            break;
          case "OD_SP":
            statoOnDemand = "ondemand_spento";
            break;
          case "false":
            statoOnDemand = "";
            break;
          default:
            break;
        }
        return statoOnDemand;
      }

      function getPercAvanzamento(sum_c, sum_non_c, count_c, count_non_c) {
        if ((sum_c + sum_non_c) == 0)
          return 0;
        return count_c * 100 / (count_c + count_non_c);
      }

      $scope.openDetail_clone = function (clone) {

        $scope.cloneSelected = clone;
        $scope.resizeGridValuesActive = {
          "mdcolssm": 2,
          "mdcolsmd": 4,
          "mdcolslg": 6,
          "mdcolsgtlg": 12,
        };
        stopInterval();

        getSPbyClone(clone.codice_clone, clone.stage);
      }

      $scope.closeDetail_clone = function () {
        $scope.cloneSelected = [];
        $scope.SPlistByClone = [];
        $scope.oggettiBySP = {
          "db2": [],
          "vsm": [],
          "seq": [],
          "gdg": []
        };
        $scope.TSlistByDBR = [];
        $scope.idSelectedSP = null;
        $scope.idSelectedDBR = null;
        $scope.resizeGridValuesActive = {
          "mdcolssm": 2,
          "mdcolsmd": 8,
          "mdcolslg": 12,
          "mdcolsgtlg": 24,
        };
        init();
      }

      $scope.selectStage = function (stageLetter) {
        $scope.stageSelected = stageLetter;
      }

      $scope.openDetail_sp = function (sp) {

        $scope.spSelected = sp;
        $scope.idSelectedSP = sp.id;
        getDBdataBySp(sp.id);
      }

      $scope.openDetail_oggetto = function (dbr) {

        $scope.dbrSelected = dbr;
        $scope.idSelectedDBR = dbr.oggetto;
        getTSbyDBR(dbr.oggetto);
      }

      $scope.isStageSelected = function () {
        if($scope.stageSelected == "")
          return true;
        else {
          return false;
        }
      }

      $scope.toggleInfo = function(){
        $scope.viewInfo = !$scope.viewInfo;
        $scope.viewInfoIcon = $scope.viewInfo ? "info" : "info_outline";
        console.log("$scope.viewInfo: ", $scope.viewInfo);
      }
    }
})();
