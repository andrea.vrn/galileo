(function(){
  'use strict';

  angular.module('app')
        .service('dashboardService', [
        '$q',
        '$http',
        'errorHandler',
        'configuration',
        dashboardService
  ]);

  function dashboardService($q, $http, errorHandler, configuration){
    
    return {
      getData : function(target) {
        var config = configuration.get('internalServices');
        var internalServicesBaseUrl = config.baseUrl;
        var req = {
          method: 'GET',
          //url: 'http://localhost:3001/' + target,
          //url: 'http://192.168.13.104/ServiceGalileo/SrvRib.svc/' + target,
          url: internalServicesBaseUrl + '/SrvRib.svc/' + target,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        };
        var promise = $http(req);
        promise.then(function(response) {
          console.log('service success', response);
        }, function(response) {
          console.log('service error', response);
          //errorHandler.notify('error loading performance data from ' + req.url);
        }, function (rejection) {
          console.error('service rejection', rejection);
          errorHandler.swallow('error loading performance data from ' + req.url);
        });
        return promise;
        //return performanceData;
      }
    };
  }

})();
