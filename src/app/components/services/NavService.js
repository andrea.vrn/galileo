(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){//per le icone vedi https://material.io/icons/
    var menuItems = [
      /*{
        name: 'Dashboard',
        icon: 'dashboard',
        sref: '.dashboard'
      },
      {
        name: 'What If',
        icon: 'view_module',
        sref: '.table'
      },*/
      {
        name: 'Input Logs',
        icon: 'assignment',
        sref: '.inputLogs'
      },
      {
        name: 'Correlations',
        icon: 'list',
        sref: '.correlations'
      },
      {
        name: 'Alarms',
        icon: 'alarm',
        sref: '.alarms'
      }
    ];

    return {
      loadAllItems : function() {
        return $q.when(menuItems);
      }
    };
  }

})();
