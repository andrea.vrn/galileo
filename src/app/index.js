'use strict';

angular.module('galileo.main', ['galileo.core', 'ngAnimate', 'ngCookies',
  'ngSanitize', 'ui.router', 'ngMaterial', 'nvd3', 'app' , 'md.data.table'])

  .constant('APP_SETUP', {
    configurationUrl: 'config/application.json',
    configurationStorageKey: 'APP_CONFIG'
  })
  /*
  * Routing configuration
  */
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '',
        templateUrl: 'app/views/main.html',
        controller: 'MainController',
        controllerAs: 'vm',
        abstract: true
      })
      .state('home.dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        controllerAs: 'vm',
        templateUrl: 'app/views/dashboard.html',
        data: {
          title: 'Dashboard'
        }
      })
      .state('home.correlations', {
        url: '/correlations',
        controller: 'CorrelationsController',
        controllerAs: 'vm',
        templateUrl: 'app/views/correlations.html',
        data: {
          title: 'Correlations'
        }
      })
      .state('home.alarms', {
        url: '/alarms',
        templateUrl: 'app/views/alarms.html',
        controller: 'AlarmsController',
        controllerAs: 'vm',
        data: {
          title: 'Alarms'
        }
      })


      .state('about', {
        url: '/about',
        templateUrl: 'app/core/about.html',
        controller: 'AboutCtrl'
      })
      .state('error', {
        url: '/error',
        templateUrl: 'app/core/error.html',
        params: {
          error: null
        },
        controller: 'ErrorCtrl'
      });
    $urlRouterProvider.otherwise('/dashboard');
  })

  /*
  * Theming configuration
  */
  .config(function ($mdThemingProvider, $mdIconProvider) {

    $mdThemingProvider.theme('default')
      .primaryPalette('grey', {
        'default': '400', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
        'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
      .accentPalette('red');

    $mdIconProvider.icon('user', 'assets/images/user.svg', 64);
  })
  .run(function($http, APP_SETUP) {
    console.log('Getting configuration from: ', APP_SETUP.configurationUrl);
    $http.get(APP_SETUP.configurationUrl).then(
      function(response) {
        sessionStorage.setItem(APP_SETUP.configurationStorageKey, JSON.stringify(response.data));
      },
      function(error) {
        console.error('error loading configuration using app setup: ', APP_SETUP, error);
      },
      function(error) {
        console.error('error loading configuration using app setup: ', APP_SETUP, error);
      });
});
