# Guida sviluppo front-end Galileo


Prerequisiti
------------

Installare NodeJS (https://nodejs.org/it/download/).

Portarsi nel path del progetto e lanciare:

```shell
$ npm install -g bower

$ npm install -g gulp

$ npm install gulp --save-dev
```


Ambiente di sviluppo
------------

Il front-end si può sviluppare con un semplice editor di testo. Suggerisco di utilizzare Atom (https://atom.io/), editor potente ed evoluto.

Nella root path del progetto lanciare:
```shell
$ npm install
```
Questo comando legge il file `package.json` e scarica tutte le dipendenze che sono specificate e che servono al portale.

Lanciare il comando
```shell
$ gulp serve
```
per lanciare un Web Server di sviluppo che viene servito di default sulla porta 3000. Il comando lancia autonomamente il browser e ogni modifica e salvataggio al codice scatena il refresh della pagina (utile per testare contemporaneamente su più browser e/o dispositivi).


Architettura progetto
------------
Il framework grafico utilizzato nel portale è AngularJS Material; questo poggia sul motore javascript AngularJS (ver. 1 https://angularjs.org/). Per demo e documentazione consultare https://material.angularjs.org/latest/
. Tutte le scelte implementative di Galileo si rifanno alle componenti software offerte da tale framework.

Il framework AngularJS Material adotta il modello MVC (Model - View - Control).

Il codice della logic del progetto si trova in `src\`.
Il browser entra dall'`index.html`, carica tutte le librerie presenti nella head del documento e a partire dal file `app.js` presente in `src\app\` inietta le viste, i controller e i servizi.

**Vista**: rappresenta ciò che l'utente vede, è implementata nei file `.html` presenti in `src\app\views`. In `dashboard.html` è presente il codice che regola tutti gli oggetti del portale.

**Modello**: rappresenta il dato che si vuole presentare.

**Controller**: coerentemente alla logica del modello, rappresenta il punto di unione tra la vista (dove l'utente agisce) e il modello (ovvero il dato che c'è dietro). Nell'alberatura del progetto sono in `src\app\controllers`: il file `DashboardController.js` rappresenta l'unico controller utilizzato dal portale.

**Servizio**: sta logicamente a fianco del controller e contiene la vera core logic del portale. Qui sono presenti le funzioni principali; nel caso di Galileo il servizio sviluppato è `src\app\components\services\DashboardService.js`, e contiene un'unica funzione (`getData()`) che è alla base del portale: l'acquisizione di dati da una sorgente.

L'interazione tra vista e controller avviene tramite l'oggetto `$scope` che costituisce il modello dell'applicazione. Costituisce il punto di contatto tra la vista (gli oggetti inseriti nella vista sono agganciati a variabili definite in `$scope`) e il controller (che agisce direttamente sulle variabile definite in `$scope`).


Scelte implementative front-end
------------

La dashboard è strutturata mediante box flessibili e responsive (si adattano autonomamente alle dimensioni dello schermo) che contengono, a seconda delle azioni dell'utente, i cloni e le tabelle in cascata di dettaglio.

Al primo livello sono presenti due box fratelli che si spartiscono metà schermo; quello di destra è regolato dalla direttiva `ng-show` che solamente quando un clone è selezionato ne mostra il contenuto. Autonomente viene quindi gestito il ridimensionamento a tutta o a metà larghezza.

Questa la struttura di primo livello:

```html
<md-content layout="row">
  <div flex ng-cloak>
    ...
  </div>
  <div flex ng-show="cloneSelected.length != 0" class="animate-show-hide">
    ...
  </div>
</md-content>
```

Nel primo div sono disegnati i cloni, nel secondo (condizionato dal click su un clone) le tabelle di dettaglio in cascata.

**Clone**: un clone è rappresentato tramite un oggetto `md-grid-tile` (si veda la demo in https://material.angularjs.org/latest/demo/gridList). Il codice che genera i cloni è realizzato mediante direttiva `ng-repeat`: questa direttiva, utilizzata anche in altre parti di Galileo, permette di generare più elementi dello stesso tipo a partire da una struttura presente nello `$scope`. Nel caso dei cloni la struttura dello `$scope` è `$scope.cloni` (si osservi che nella vista non è specificato il prefisso `$scope.` mentre nel controller sì).
L'evento di click di un clone è intercettato dalla direttiva `ng-click` che invoca il metodo specificato.

**Dettaglio in cascata**: dopo la selezione di un clone viene invocato il metodo `openDetail_clone(clone)` (chiamata `getSPbyClone(clone.codice_clone, clone.stage)` che invoca il metodo esposto in http://services_address/WCFServicesGalileo/SrvRib.svc/GetSPbyClone) che popola la struttura `$scope.SPlistByClone`. Tale struttura viene utilizzata per disegnare la prima tabella relativa alle richieste Sharepoint (ancora aperte e chiuse) relative al clone selezionato.

Dopo la selezione di una richiesta viene invocato il metodo `openDetail_sp(sp)` (chiamata `getDBdataBySp(sp.id)` che invoca il metodo esposto in http://services_address/WCFServicesGalileo/SrvRib.svc/GetDBdataBySp) che popola la struttura `$scope.oggettiBySP`. Tale struttura è costituita da quattro sottocampi:
```js
$scope.oggettiBySP = {
  "db2": [],
  "vsm": [],
  "seq": [],
  "gdg": []
};
```
e viene utilizzata per disegnare la terza tabella (sono in realtà quatto tabelle divise in quattro tab a seconda del tipo di oggetto) relativa agli oggetti in copia nella richiesta Sharepoint selezionata.

Solo su selezione di un oggetto di tipo DB2 viene invocato il metodo `openDetail_oggetto(oggetto)` (chiamata `getTSbyDBR(dbr.oggetto)` che invoca il metodo esposto in http://services_address/WCFServicesGalileo/SrvRib.svc/GetTSByDBR) che popola la struttura `$scope.TSlistByDBR`. Tale struttura viene utilizzata per disegnare l'ultima tabella della cascata con i tablespace relativi all'oggetto DBR selezionato.

Build e deployment
------------

Quando si vuole deployare una nuova versione, va lanciato il comando
```shell
$ gulp build
```
che builda tutta la soluzione in `dist\`. Prendere tutto il contenuto di questa cartella e portarlo su IIS per il deployment.

### Appendice
Vedi https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet per markdown.
