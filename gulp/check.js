'use strict';

const jshint = require('gulp-jshint');
const gulp   = require('gulp');
const stylish = require('jshint-stylish');

gulp.task('lint', function() {
  return gulp.src('./src/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});
